﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    internal class Transaction
    {
        public String hash;
        public String srcAddress;
        public String finalAddress;
        private String signature;
        public DateTime timeStamp;
        public double amount;
        public double transaFee;

        // Determines a transaction's category
        public Category transactionCategory;

        // The four horsemen of transanctions
        public enum Category
        {
            DEPOSIT,
            WITHDRAWAL,
            REWARD,
            STANDARD
        }

        // Transaction Constructor
        public Transaction(string srcAddress, string dstAddress, double amount, double fee, string privateKey)
        {
            this.srcAddress = srcAddress;
            this.finalAddress = dstAddress;
            this.amount = amount;
            this.transaFee = fee;

            this.timeStamp = DateTime.Now;

            this.hash = createHash();
            this.signature = Wallet.Wallet.CreateSignature(srcAddress, privateKey, this.hash);
        }

        // Reimplementation of utility function
        public String createHash()
        {
            String hash = String.Empty;

            SHA256 hasher = SHA256Managed.Create();
            String hashInput = timeStamp.ToString() + srcAddress.ToString() + finalAddress.ToString() + amount.ToString() + transaFee.ToString();

            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(hashInput)); 

            // Converting hash from byte array to string and formatting doubles
            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }

            return hash;
        }

        public override string ToString()
        {
            return "\tCategory: " + transactionCategory + "\n" +
                    "\t\t Digital Signature: " + signature + "\n" +
                    "\t\t Timestamp: " + timeStamp.ToString() + "\n" +
                    "\t\t Hash: " + hash + "\n" +
                    "\t\t Transferred: " + "₦" + amount.ToString() + "\n" +
                    "\t\t Fees: " + transaFee.ToString() + "\n" +
                    "\t\t Source Address: " + srcAddress + "\n" +
                    "\t\t Destination Address: " + finalAddress + "\n";
        }
    }
}