﻿namespace BlockchainAssignment
{
    partial class BlockchainApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.printerConsole = new System.Windows.Forms.RichTextBox();
            this.printButton = new System.Windows.Forms.Button();
            this.blockIndexTextBox = new System.Windows.Forms.TextBox();
            this.createWalletButton = new System.Windows.Forms.Button();
            this.publicKeyTextBox = new System.Windows.Forms.TextBox();
            this.privateKeyTextBox = new System.Windows.Forms.TextBox();
            this.publicKeyLabel = new System.Windows.Forms.Label();
            this.privateKeyLabel = new System.Windows.Forms.Label();
            this.checkKeysButton = new System.Windows.Forms.Button();
            this.newTransactionButton = new System.Windows.Forms.Button();
            this.blockIndexLabel = new System.Windows.Forms.Label();
            this.amountTextBox = new System.Windows.Forms.TextBox();
            this.feeTextBox = new System.Windows.Forms.TextBox();
            this.amountLabel = new System.Windows.Forms.Label();
            this.feeLabel = new System.Windows.Forms.Label();
            this.receiverKeyTextBox = new System.Windows.Forms.TextBox();
            this.receiverKeyLabel = new System.Windows.Forms.Label();
            this.newBlockButton = new System.Windows.Forms.Button();
            this.showAllButton = new System.Windows.Forms.Button();
            this.pendingTransactionsButton = new System.Windows.Forms.Button();
            this.depositButton = new System.Windows.Forms.Button();
            this.depositLabel = new System.Windows.Forms.Label();
            this.depositTextBox = new System.Windows.Forms.TextBox();
            this.checkBalanceButton = new System.Windows.Forms.Button();
            this.horizontalLabelDivider = new System.Windows.Forms.Label();
            this.fileMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.openMenuItemButton = new System.Windows.Forms.ToolStripMenuItem();
            this.openRecentMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.dummytxtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dummy2txtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dummy3txtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addressPreferenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altruisticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greedyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.randomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invalidateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invalidBlockHashToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invalidBlockPreviousHashToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.withdrawalAmountLabel = new System.Windows.Forms.Label();
            this.withdrawalTextBox = new System.Windows.Forms.TextBox();
            this.withdrawalButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.githToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miningPreferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altruisticToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.greedyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.preferredAddressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.randomToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.validation = new System.Windows.Forms.Button();
            this.avgTime = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // printerConsole
            // 
            this.printerConsole.BackColor = System.Drawing.SystemColors.InfoText;
            this.printerConsole.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.printerConsole.Location = new System.Drawing.Point(22, 41);
            this.printerConsole.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.printerConsole.Name = "printerConsole";
            this.printerConsole.Size = new System.Drawing.Size(1305, 672);
            this.printerConsole.TabIndex = 0;
            this.printerConsole.Text = "";
            // 
            // printButton
            // 
            this.printButton.Location = new System.Drawing.Point(1339, 389);
            this.printButton.Margin = new System.Windows.Forms.Padding(4);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(130, 26);
            this.printButton.TabIndex = 1;
            this.printButton.Text = "Block Info";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // blockIndexTextBox
            // 
            this.blockIndexTextBox.Location = new System.Drawing.Point(1491, 391);
            this.blockIndexTextBox.Name = "blockIndexTextBox";
            this.blockIndexTextBox.Size = new System.Drawing.Size(37, 25);
            this.blockIndexTextBox.TabIndex = 2;
            // 
            // createWalletButton
            // 
            this.createWalletButton.Location = new System.Drawing.Point(1335, 41);
            this.createWalletButton.Name = "createWalletButton";
            this.createWalletButton.Size = new System.Drawing.Size(150, 56);
            this.createWalletButton.TabIndex = 3;
            this.createWalletButton.Text = "Create New Wallet";
            this.createWalletButton.UseVisualStyleBackColor = true;
            this.createWalletButton.Click += new System.EventHandler(this.createWalletButton_Click);
            // 
            // publicKeyTextBox
            // 
            this.publicKeyTextBox.Location = new System.Drawing.Point(1491, 41);
            this.publicKeyTextBox.Name = "publicKeyTextBox";
            this.publicKeyTextBox.Size = new System.Drawing.Size(182, 25);
            this.publicKeyTextBox.TabIndex = 4;
            // 
            // privateKeyTextBox
            // 
            this.privateKeyTextBox.Location = new System.Drawing.Point(1491, 72);
            this.privateKeyTextBox.Name = "privateKeyTextBox";
            this.privateKeyTextBox.Size = new System.Drawing.Size(182, 25);
            this.privateKeyTextBox.TabIndex = 5;
            // 
            // publicKeyLabel
            // 
            this.publicKeyLabel.AutoSize = true;
            this.publicKeyLabel.BackColor = System.Drawing.Color.DarkGray;
            this.publicKeyLabel.ForeColor = System.Drawing.Color.Black;
            this.publicKeyLabel.Location = new System.Drawing.Point(1679, 47);
            this.publicKeyLabel.Name = "publicKeyLabel";
            this.publicKeyLabel.Size = new System.Drawing.Size(71, 19);
            this.publicKeyLabel.TabIndex = 6;
            this.publicKeyLabel.Text = "Public Key";
            // 
            // privateKeyLabel
            // 
            this.privateKeyLabel.AutoSize = true;
            this.privateKeyLabel.BackColor = System.Drawing.Color.DarkGray;
            this.privateKeyLabel.ForeColor = System.Drawing.Color.Black;
            this.privateKeyLabel.Location = new System.Drawing.Point(1679, 78);
            this.privateKeyLabel.Name = "privateKeyLabel";
            this.privateKeyLabel.Size = new System.Drawing.Size(77, 19);
            this.privateKeyLabel.TabIndex = 7;
            this.privateKeyLabel.Text = "Private Key";
            // 
            // checkKeysButton
            // 
            this.checkKeysButton.Location = new System.Drawing.Point(1640, 103);
            this.checkKeysButton.Name = "checkKeysButton";
            this.checkKeysButton.Size = new System.Drawing.Size(121, 45);
            this.checkKeysButton.TabIndex = 8;
            this.checkKeysButton.Text = "Check Key";
            this.checkKeysButton.UseVisualStyleBackColor = true;
            this.checkKeysButton.Click += new System.EventHandler(this.checkKeysButton_Click);
            // 
            // newTransactionButton
            // 
            this.newTransactionButton.Location = new System.Drawing.Point(1335, 212);
            this.newTransactionButton.Name = "newTransactionButton";
            this.newTransactionButton.Size = new System.Drawing.Size(150, 56);
            this.newTransactionButton.TabIndex = 10;
            this.newTransactionButton.Text = "New Transaction";
            this.newTransactionButton.UseVisualStyleBackColor = true;
            this.newTransactionButton.Click += new System.EventHandler(this.newTransactionButton_Click);
            // 
            // blockIndexLabel
            // 
            this.blockIndexLabel.AutoSize = true;
            this.blockIndexLabel.BackColor = System.Drawing.Color.DarkGray;
            this.blockIndexLabel.ForeColor = System.Drawing.Color.Black;
            this.blockIndexLabel.Location = new System.Drawing.Point(1534, 396);
            this.blockIndexLabel.Name = "blockIndexLabel";
            this.blockIndexLabel.Size = new System.Drawing.Size(42, 19);
            this.blockIndexLabel.TabIndex = 11;
            this.blockIndexLabel.Text = "Index";
            // 
            // amountTextBox
            // 
            this.amountTextBox.Location = new System.Drawing.Point(1491, 212);
            this.amountTextBox.Name = "amountTextBox";
            this.amountTextBox.Size = new System.Drawing.Size(72, 25);
            this.amountTextBox.TabIndex = 12;
            // 
            // feeTextBox
            // 
            this.feeTextBox.Location = new System.Drawing.Point(1491, 243);
            this.feeTextBox.Name = "feeTextBox";
            this.feeTextBox.Size = new System.Drawing.Size(72, 25);
            this.feeTextBox.TabIndex = 13;
            // 
            // amountLabel
            // 
            this.amountLabel.AutoSize = true;
            this.amountLabel.BackColor = System.Drawing.Color.DarkGray;
            this.amountLabel.ForeColor = System.Drawing.Color.Black;
            this.amountLabel.Location = new System.Drawing.Point(1569, 215);
            this.amountLabel.Name = "amountLabel";
            this.amountLabel.Size = new System.Drawing.Size(59, 19);
            this.amountLabel.TabIndex = 14;
            this.amountLabel.Text = "Amount";
            // 
            // feeLabel
            // 
            this.feeLabel.AutoSize = true;
            this.feeLabel.BackColor = System.Drawing.Color.DarkGray;
            this.feeLabel.ForeColor = System.Drawing.Color.Black;
            this.feeLabel.Location = new System.Drawing.Point(1569, 246);
            this.feeLabel.Name = "feeLabel";
            this.feeLabel.Size = new System.Drawing.Size(30, 19);
            this.feeLabel.TabIndex = 15;
            this.feeLabel.Text = "Fee";
            // 
            // receiverKeyTextBox
            // 
            this.receiverKeyTextBox.Location = new System.Drawing.Point(1339, 336);
            this.receiverKeyTextBox.Name = "receiverKeyTextBox";
            this.receiverKeyTextBox.Size = new System.Drawing.Size(224, 25);
            this.receiverKeyTextBox.TabIndex = 16;
            // 
            // receiverKeyLabel
            // 
            this.receiverKeyLabel.AutoSize = true;
            this.receiverKeyLabel.BackColor = System.Drawing.Color.DarkGray;
            this.receiverKeyLabel.ForeColor = System.Drawing.Color.Black;
            this.receiverKeyLabel.Location = new System.Drawing.Point(1569, 342);
            this.receiverKeyLabel.Name = "receiverKeyLabel";
            this.receiverKeyLabel.Size = new System.Drawing.Size(85, 19);
            this.receiverKeyLabel.TabIndex = 17;
            this.receiverKeyLabel.Text = "Receiver Key";
            // 
            // newBlockButton
            // 
            this.newBlockButton.Location = new System.Drawing.Point(1340, 422);
            this.newBlockButton.Name = "newBlockButton";
            this.newBlockButton.Size = new System.Drawing.Size(129, 73);
            this.newBlockButton.TabIndex = 9;
            this.newBlockButton.Text = "New Block";
            this.newBlockButton.UseVisualStyleBackColor = true;
            this.newBlockButton.Click += new System.EventHandler(this.newBlockButton_Click);
            // 
            // showAllButton
            // 
            this.showAllButton.Location = new System.Drawing.Point(1621, 451);
            this.showAllButton.Name = "showAllButton";
            this.showAllButton.Size = new System.Drawing.Size(129, 44);
            this.showAllButton.TabIndex = 18;
            this.showAllButton.Text = "Show All";
            this.showAllButton.UseVisualStyleBackColor = true;
            this.showAllButton.Click += new System.EventHandler(this.showAllButton_Click);
            // 
            // pendingTransactionsButton
            // 
            this.pendingTransactionsButton.Location = new System.Drawing.Point(1339, 274);
            this.pendingTransactionsButton.Name = "pendingTransactionsButton";
            this.pendingTransactionsButton.Size = new System.Drawing.Size(146, 56);
            this.pendingTransactionsButton.TabIndex = 19;
            this.pendingTransactionsButton.Text = "Pending Transactions";
            this.pendingTransactionsButton.UseVisualStyleBackColor = true;
            this.pendingTransactionsButton.Click += new System.EventHandler(this.pendingTransactionsButton_Click);
            // 
            // depositButton
            // 
            this.depositButton.Location = new System.Drawing.Point(1335, 106);
            this.depositButton.Name = "depositButton";
            this.depositButton.Size = new System.Drawing.Size(150, 42);
            this.depositButton.TabIndex = 20;
            this.depositButton.Text = "Deposit";
            this.depositButton.UseVisualStyleBackColor = true;
            this.depositButton.Click += new System.EventHandler(this.depositButton_Click);
            // 
            // depositLabel
            // 
            this.depositLabel.AutoSize = true;
            this.depositLabel.BackColor = System.Drawing.Color.DarkGray;
            this.depositLabel.ForeColor = System.Drawing.Color.Black;
            this.depositLabel.Location = new System.Drawing.Point(1569, 119);
            this.depositLabel.Name = "depositLabel";
            this.depositLabel.Size = new System.Drawing.Size(59, 19);
            this.depositLabel.TabIndex = 22;
            this.depositLabel.Text = "Amount";
            // 
            // depositTextBox
            // 
            this.depositTextBox.Location = new System.Drawing.Point(1491, 114);
            this.depositTextBox.Name = "depositTextBox";
            this.depositTextBox.Size = new System.Drawing.Size(72, 25);
            this.depositTextBox.TabIndex = 21;
            // 
            // checkBalanceButton
            // 
            this.checkBalanceButton.Location = new System.Drawing.Point(1491, 274);
            this.checkBalanceButton.Name = "checkBalanceButton";
            this.checkBalanceButton.Size = new System.Drawing.Size(146, 56);
            this.checkBalanceButton.TabIndex = 23;
            this.checkBalanceButton.Text = "Check Balance";
            this.checkBalanceButton.UseVisualStyleBackColor = true;
            this.checkBalanceButton.Click += new System.EventHandler(this.checkBalanceButton_Click);
            // 
            // horizontalLabelDivider
            // 
            this.horizontalLabelDivider.BackColor = System.Drawing.Color.Coral;
            this.horizontalLabelDivider.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.horizontalLabelDivider.Location = new System.Drawing.Point(1335, 199);
            this.horizontalLabelDivider.Name = "horizontalLabelDivider";
            this.horizontalLabelDivider.Size = new System.Drawing.Size(432, 10);
            this.horizontalLabelDivider.TabIndex = 24;
            // 
            // fileMenuButton
            // 
            this.fileMenuButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openMenuItemButton,
            this.openRecentMenuButton,
            this.toolStripSeparator1,
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripSeparator2,
            this.toolStripMenuItem4});
            this.fileMenuButton.Name = "fileMenuButton";
            this.fileMenuButton.Size = new System.Drawing.Size(37, 20);
            this.fileMenuButton.Text = "File";
            // 
            // openMenuItemButton
            // 
            this.openMenuItemButton.Name = "openMenuItemButton";
            this.openMenuItemButton.Size = new System.Drawing.Size(151, 22);
            this.openMenuItemButton.Text = "Open";
            // 
            // openRecentMenuButton
            // 
            this.openRecentMenuButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dummytxtToolStripMenuItem,
            this.dummy2txtToolStripMenuItem,
            this.dummy3txtToolStripMenuItem});
            this.openRecentMenuButton.Name = "openRecentMenuButton";
            this.openRecentMenuButton.Size = new System.Drawing.Size(151, 22);
            this.openRecentMenuButton.Text = "Open Recent";
            // 
            // dummytxtToolStripMenuItem
            // 
            this.dummytxtToolStripMenuItem.Name = "dummytxtToolStripMenuItem";
            this.dummytxtToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.dummytxtToolStripMenuItem.Text = "Dummy.txt";
            // 
            // dummy2txtToolStripMenuItem
            // 
            this.dummy2txtToolStripMenuItem.Name = "dummy2txtToolStripMenuItem";
            this.dummy2txtToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.dummy2txtToolStripMenuItem.Text = "Dummy2,txt";
            // 
            // dummy3txtToolStripMenuItem
            // 
            this.dummy3txtToolStripMenuItem.Name = "dummy3txtToolStripMenuItem";
            this.dummy3txtToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.dummy3txtToolStripMenuItem.Text = "Dummy3,txt";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(148, 6);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(151, 22);
            this.toolStripMenuItem1.Text = "Save";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(151, 22);
            this.toolStripMenuItem2.Text = "Save As";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(148, 6);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(151, 22);
            this.toolStripMenuItem4.Text = "Quit";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(39, 20);
            this.toolStripMenuItem3.Text = "Edit";
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.preferencesToolStripMenuItem.Text = "Mining Preferences";
            // 
            // addressPreferenceToolStripMenuItem
            // 
            this.addressPreferenceToolStripMenuItem.Name = "addressPreferenceToolStripMenuItem";
            this.addressPreferenceToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addressPreferenceToolStripMenuItem.Text = "Address Preference";
            // 
            // altruisticToolStripMenuItem
            // 
            this.altruisticToolStripMenuItem.Name = "altruisticToolStripMenuItem";
            this.altruisticToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.altruisticToolStripMenuItem.Text = "Altruistic";
            // 
            // greedyToolStripMenuItem
            // 
            this.greedyToolStripMenuItem.Name = "greedyToolStripMenuItem";
            this.greedyToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.greedyToolStripMenuItem.Text = "Greedy";
            // 
            // randomToolStripMenuItem
            // 
            this.randomToolStripMenuItem.Name = "randomToolStripMenuItem";
            this.randomToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.randomToolStripMenuItem.Text = "Random";
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.invalidateToolStripMenuItem});
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.testToolStripMenuItem.Text = "Tools";
            // 
            // invalidateToolStripMenuItem
            // 
            this.invalidateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.invalidBlockHashToolStripMenuItem,
            this.invalidBlockPreviousHashToolStripMenuItem});
            this.invalidateToolStripMenuItem.Name = "invalidateToolStripMenuItem";
            this.invalidateToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.invalidateToolStripMenuItem.Text = "Tests";
            // 
            // invalidBlockHashToolStripMenuItem
            // 
            this.invalidBlockHashToolStripMenuItem.Name = "invalidBlockHashToolStripMenuItem";
            this.invalidBlockHashToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.invalidBlockHashToolStripMenuItem.Text = "Invalid Block - Hash";
            // 
            // invalidBlockPreviousHashToolStripMenuItem
            // 
            this.invalidBlockPreviousHashToolStripMenuItem.Name = "invalidBlockPreviousHashToolStripMenuItem";
            this.invalidBlockPreviousHashToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.invalidBlockPreviousHashToolStripMenuItem.Text = "Invalid Block - Previous Hash";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // withdrawalAmountLabel
            // 
            this.withdrawalAmountLabel.AutoSize = true;
            this.withdrawalAmountLabel.BackColor = System.Drawing.Color.DarkGray;
            this.withdrawalAmountLabel.ForeColor = System.Drawing.Color.Black;
            this.withdrawalAmountLabel.Location = new System.Drawing.Point(1569, 167);
            this.withdrawalAmountLabel.Name = "withdrawalAmountLabel";
            this.withdrawalAmountLabel.Size = new System.Drawing.Size(59, 19);
            this.withdrawalAmountLabel.TabIndex = 35;
            this.withdrawalAmountLabel.Text = "Amount";
            // 
            // withdrawalTextBox
            // 
            this.withdrawalTextBox.Location = new System.Drawing.Point(1491, 162);
            this.withdrawalTextBox.Name = "withdrawalTextBox";
            this.withdrawalTextBox.Size = new System.Drawing.Size(72, 25);
            this.withdrawalTextBox.TabIndex = 34;
            // 
            // withdrawalButton
            // 
            this.withdrawalButton.Location = new System.Drawing.Point(1335, 154);
            this.withdrawalButton.Name = "withdrawalButton";
            this.withdrawalButton.Size = new System.Drawing.Size(150, 42);
            this.withdrawalButton.TabIndex = 33;
            this.withdrawalButton.Text = "Withdraw";
            this.withdrawalButton.UseVisualStyleBackColor = true;
            this.withdrawalButton.Click += new System.EventHandler(this.withdrawalButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.githToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1773, 25);
            this.menuStrip1.TabIndex = 32;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // githToolStripMenuItem
            // 
            this.githToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quitToolStripMenuItem});
            this.githToolStripMenuItem.Name = "githToolStripMenuItem";
            this.githToolStripMenuItem.Size = new System.Drawing.Size(39, 21);
            this.githToolStripMenuItem.Text = "File";
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miningPreferencesToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(42, 21);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // miningPreferencesToolStripMenuItem
            // 
            this.miningPreferencesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.altruisticToolStripMenuItem1,
            this.greedyToolStripMenuItem1,
            this.preferredAddressToolStripMenuItem,
            this.randomToolStripMenuItem1});
            this.miningPreferencesToolStripMenuItem.Name = "miningPreferencesToolStripMenuItem";
            this.miningPreferencesToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.miningPreferencesToolStripMenuItem.Text = "Mining Preferences";
            // 
            // altruisticToolStripMenuItem1
            // 
            this.altruisticToolStripMenuItem1.Name = "altruisticToolStripMenuItem1";
            this.altruisticToolStripMenuItem1.Size = new System.Drawing.Size(183, 22);
            this.altruisticToolStripMenuItem1.Text = "Altruistic";
            this.altruisticToolStripMenuItem1.Click += new System.EventHandler(this.altruisticToolStripMenuItem1_Click);
            // 
            // greedyToolStripMenuItem1
            // 
            this.greedyToolStripMenuItem1.Name = "greedyToolStripMenuItem1";
            this.greedyToolStripMenuItem1.Size = new System.Drawing.Size(183, 22);
            this.greedyToolStripMenuItem1.Text = "Greedy";
            this.greedyToolStripMenuItem1.Click += new System.EventHandler(this.greedyToolStripMenuItem1_Click);
            // 
            // preferredAddressToolStripMenuItem
            // 
            this.preferredAddressToolStripMenuItem.Name = "preferredAddressToolStripMenuItem";
            this.preferredAddressToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.preferredAddressToolStripMenuItem.Text = "Preferred Address";
            this.preferredAddressToolStripMenuItem.Click += new System.EventHandler(this.preferredAddressToolStripMenuItem_Click);
            // 
            // randomToolStripMenuItem1
            // 
            this.randomToolStripMenuItem1.Name = "randomToolStripMenuItem1";
            this.randomToolStripMenuItem1.Size = new System.Drawing.Size(183, 22);
            this.randomToolStripMenuItem1.Text = "Random";
            this.randomToolStripMenuItem1.Click += new System.EventHandler(this.randomToolStripMenuItem1_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Coral;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(1329, 364);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(432, 10);
            this.label1.TabIndex = 37;
            // 
            // validation
            // 
            this.validation.Location = new System.Drawing.Point(1491, 586);
            this.validation.Name = "validation";
            this.validation.Size = new System.Drawing.Size(137, 41);
            this.validation.TabIndex = 38;
            this.validation.Text = "VALIDATION";
            this.validation.UseVisualStyleBackColor = true;
            this.validation.Click += new System.EventHandler(this.validation_Click);
            // 
            // avgTime
            // 
            this.avgTime.Location = new System.Drawing.Point(1491, 516);
            this.avgTime.Name = "avgTime";
            this.avgTime.Size = new System.Drawing.Size(137, 45);
            this.avgTime.TabIndex = 39;
            this.avgTime.Text = "Average Time";
            this.avgTime.UseVisualStyleBackColor = true;
            this.avgTime.Click += new System.EventHandler(this.avgTime_Click);
            // 
            // BlockchainApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(1773, 742);
            this.Controls.Add(this.avgTime);
            this.Controls.Add(this.validation);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.withdrawalAmountLabel);
            this.Controls.Add(this.withdrawalTextBox);
            this.Controls.Add(this.withdrawalButton);
            this.Controls.Add(this.horizontalLabelDivider);
            this.Controls.Add(this.checkBalanceButton);
            this.Controls.Add(this.depositLabel);
            this.Controls.Add(this.depositTextBox);
            this.Controls.Add(this.depositButton);
            this.Controls.Add(this.pendingTransactionsButton);
            this.Controls.Add(this.showAllButton);
            this.Controls.Add(this.receiverKeyLabel);
            this.Controls.Add(this.receiverKeyTextBox);
            this.Controls.Add(this.feeLabel);
            this.Controls.Add(this.amountLabel);
            this.Controls.Add(this.feeTextBox);
            this.Controls.Add(this.amountTextBox);
            this.Controls.Add(this.blockIndexLabel);
            this.Controls.Add(this.newTransactionButton);
            this.Controls.Add(this.newBlockButton);
            this.Controls.Add(this.checkKeysButton);
            this.Controls.Add(this.privateKeyLabel);
            this.Controls.Add(this.publicKeyLabel);
            this.Controls.Add(this.privateKeyTextBox);
            this.Controls.Add(this.publicKeyTextBox);
            this.Controls.Add(this.createWalletButton);
            this.Controls.Add(this.blockIndexTextBox);
            this.Controls.Add(this.printButton);
            this.Controls.Add(this.printerConsole);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.Name = "BlockchainApp";
            this.Text = "Blockchain App";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox printerConsole;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.TextBox blockIndexTextBox;
        private System.Windows.Forms.Button createWalletButton;
        private System.Windows.Forms.TextBox publicKeyTextBox;
        private System.Windows.Forms.TextBox privateKeyTextBox;
        private System.Windows.Forms.Label publicKeyLabel;
        private System.Windows.Forms.Label privateKeyLabel;
        private System.Windows.Forms.Button checkKeysButton;
        private System.Windows.Forms.Button newTransactionButton;
        private System.Windows.Forms.Label blockIndexLabel;
        private System.Windows.Forms.TextBox amountTextBox;
        private System.Windows.Forms.TextBox feeTextBox;
        private System.Windows.Forms.Label amountLabel;
        private System.Windows.Forms.Label feeLabel;
        private System.Windows.Forms.TextBox receiverKeyTextBox;
        private System.Windows.Forms.Label receiverKeyLabel;
        private System.Windows.Forms.Button newBlockButton;
        private System.Windows.Forms.Button showAllButton;
        private System.Windows.Forms.Button pendingTransactionsButton;
        private System.Windows.Forms.Button depositButton;
        private System.Windows.Forms.Label depositLabel;
        private System.Windows.Forms.TextBox depositTextBox;
        private System.Windows.Forms.Button checkBalanceButton;
        private System.Windows.Forms.Label horizontalLabelDivider;
        private System.Windows.Forms.ToolStripMenuItem fileMenuButton;
        private System.Windows.Forms.ToolStripMenuItem openMenuItemButton;
        private System.Windows.Forms.ToolStripMenuItem openRecentMenuButton;
        private System.Windows.Forms.ToolStripMenuItem dummytxtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dummy2txtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dummy3txtToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label withdrawalAmountLabel;
        private System.Windows.Forms.TextBox withdrawalTextBox;
        private System.Windows.Forms.Button withdrawalButton;
        private System.Windows.Forms.ToolStripMenuItem invalidateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invalidBlockHashToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invalidBlockPreviousHashToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addressPreferenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altruisticToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greedyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem randomToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem githToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miningPreferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altruisticToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem greedyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem preferredAddressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem randomToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button validation;
        private System.Windows.Forms.Button avgTime;
    }
}

